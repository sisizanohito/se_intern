﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_AIToMoveClick : MonoBehaviour
{

    public float remainingDistance = 0.2f;

    public Transform[] locationPoints;
    private Vector3 beforPoint;
    private int nowpoint;
    private NavMeshAgent2D agent;

    public bool isWait = true;
    public float waitTime = 0.0f;
    private bool waitFlag = false;
    private float waitCount = 0.0f;

    public bool isAimPlayer = false;
    public bool isForget = true;
    public float forgetTime = 1.0f;
    public bool playerAimFlag = false;
    private float forgetCount = 0.0f;

    private Transform player;



    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent2D>();

        agent.autoBraking = false;

        GotoNextPoint();

        if (isAimPlayer)
        {
            player = GameObject.FindWithTag("Player").transform;
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (playerAimFlag)
        {
            if (isForget)
            {
                if (forgetCount >= forgetTime)
                {
                    forgetCount = 0.0f;
                    LostPlayer();

                }
                else
                {
                    forgetCount += Time.deltaTime;
                }
            }
            else
            {
                Invoke("FindePlayer", 1.0f);
            }
            return;
        }

        if (isWait)
        {
            if (waitFlag)
            {
                if (waitCount >= waitTime)
                {
                    waitCount = 0.0f;
                    waitFlag = false;
                    agent.Resume();
                }
                else
                {
                    waitCount += Time.deltaTime;
                }
            }
            else
            {
                if (agent.remainingDistance < remainingDistance)
                {
                    GotoNextPoint();
                    waitFlag = true;
                    agent.Stop();
                }
            }
        }
        else
        {
            if (agent.remainingDistance < remainingDistance)
            {
                GotoNextPoint();
            }
        }


    }

    void GotoNextPoint()
    {
        if (locationPoints.Length == 0)
            return;
        agent.destination = locationPoints[nowpoint].position;


        nowpoint = (nowpoint + 1) % locationPoints.Length;
    }

    void FindePlayer()
    {
        if (!(playerAimFlag))
        {
            beforPoint = this.transform.position;
        }
        playerAimFlag = true;
        forgetCount = 0.0f;
        agent.destination = player.position;
        agent.Resume();
    }

    void LostPlayer()
    {
        playerAimFlag = false;
        forgetCount = 0.0f;
        waitCount = 0.0f;
        agent.destination = beforPoint;
        waitFlag = true;
        agent.Stop();
        Debug.Log("lostPlayer");
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{

    //    if (isAimPlayer)
    //    {
    //        if (collision.gameObject.tag == "Wave")
    //        {
    //            FindePlayer();
    //        }
    //    }


    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isAimPlayer)
        {
            if (collision.gameObject.tag == "Wave")
            {
                FindePlayer();
            }
        }
    }
}
