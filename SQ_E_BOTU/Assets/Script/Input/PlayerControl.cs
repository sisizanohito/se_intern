﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PlayerControl : MonoBehaviour
{

    [SerializeField] private float Speed = 0.04f, DashSpeed = 0.1f;
    private float fSpeed;
    private Vector3 Position;
    private Rigidbody2D rigidbody2d;
    private float targetAngle;
    public bool GoalFlag;

    public float GetSpeed { get { return Speed; } }
    public float GetDashSpeed { get { return DashSpeed; } }
    public float GetfSpeed { get { return fSpeed; } }
    // Use this for initialization
    void Start()
    {
        GoalFlag = false;
        Position = gameObject.transform.position;
        rigidbody2d = GetComponent<Rigidbody2D>();
        rigidbody2d.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (GoalFlag) { return; }
        rigidbody2d.MovePosition(Position);//移動
    }

    void Update()
    {
        Position = gameObject.transform.position;
        fSpeed = 0.0f;
        if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Moved || InputMnager.Instance.Phase == InputMnager.UTouchPhase.Stationary)//スワイプ中
        {
            fSpeed = Mathf.Lerp(0, Speed, InputMnager.Instance.SwipeSpeed);
            if (InputMnager.Instance.SwipeSpeed > 1)//ダッシュ処理
            {
                fSpeed = Mathf.Lerp(Speed, DashSpeed, (InputMnager.Instance.SwipeSpeed) - 1);
                //Debug.Log("ダッシュ:"+ fSpeed);
            }

            targetAngle = Vector2.Angle(InputMnager.Instance.MovePosition.normalized, transform.up) / 5;
            Vector3 cross = Vector3.Cross(InputMnager.Instance.MovePosition.normalized, transform.up);

            if (cross.z > 0)
            {
                targetAngle = 360 - targetAngle;
            }

            transform.Rotate(0, 0, targetAngle);
            Position += (Vector3)InputMnager.Instance.MovePosition.normalized * fSpeed;
        }

        /*
        #if UNITY_EDITOR
        if (EditorApplication.isPlaying)
        {
            if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Began)
            {
                Debug.Log("タッチしたよ");
            }
            if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Ended)
            {
                Debug.Log("タッチ終わったよ");
            }
            if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Moved)
            {
                Debug.Log("動いてるよ");
            }
            if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Stationary)
            {
                Debug.Log("止まってるよ");
            }
        }
        #endif
        */
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!(GoalFlag))
        {
            if (collision.transform.tag == "Enemy")
            {
                GoalFlag = true;
                rigidbody2d.isKinematic = true;
                SceneChanger.Instance.ReStart();
            }
        }

    }
}
