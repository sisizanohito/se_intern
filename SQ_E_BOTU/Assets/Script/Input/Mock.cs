﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mock : MonoBehaviour {
    public Vector3 Position, Tposition;
    public Vector3 AddPositiom;
    public const int Count = 10;
    public int now_count;
    // Use this for initialization
    void Start () {
        Tposition = new Vector3(0,0,0);
        AddPositiom = new Vector3(0, 0, 0);
        now_count = 0;
    }
	
	// Update is called once per frame
	void Update () {
        Position = gameObject.transform.position;
        if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Began)
        {
            Tposition = InputMnager.Instance.Position;
            Tposition.z = 0;
            Debug.Log(Tposition);
            AddPositiom = (Tposition-Position) /Count;
            now_count = 0;
        }
        if (now_count < Count)
        {
            Position += AddPositiom;
            now_count++;
        }
        
        gameObject.transform.position = Position;


        if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Began)
        {
            Debug.Log("タッチしたよ");
        }
        if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Ended)
        {
            Debug.Log("タッチ終わったよ");
        }
        if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Moved)
        {
            Debug.Log("動いてるよ");
        }
        if (InputMnager.Instance.Phase == InputMnager.UTouchPhase.Stationary)
        {
            Debug.Log("止まってるよ");
        }

    }
}
