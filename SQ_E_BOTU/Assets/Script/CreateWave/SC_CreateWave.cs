﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_CreateWave : MonoBehaviour {


    public GameObject VertexScript;
    public float liveTime = 2.0f; //この数値を変えれば輪の大きさが変わる
    private float time_S = 0.0f;

    public int VertexCnt = 40;  //頂点の数
    private Vector3[] pos;
    private GameObject[] Vertexes = new GameObject[64];

    LineRenderer render;

    // Use this for initialization
    void Start () {
        render = this.gameObject.GetComponent<LineRenderer>();
        pos = new Vector3[64];
        render.SetVertexCount(VertexCnt);
        for (int i = 0; i < VertexCnt; i++)
        {
            Vertexes[i] = Instantiate(VertexScript, this.transform.position, Quaternion.Euler(0.0f, 0.0f, 360.0f * i / VertexCnt));
            Vertexes[i].transform.parent = this.gameObject.transform;
        }
        Invoke("DestroyTime", liveTime); //ここで波紋の大きさを変更する」

    }
	
	// Update is called once per frame
	void Update () {
        //render = this.gameObject.GetComponent<LineRenderer>();
        for (int i = 0; i < VertexCnt; i++)
        {
           //render.SetPosition(i, Vertexes[i].transform.position);
            pos[i] = Vertexes[i].transform.position;
        }

        render.SetPositions(pos);

        /*以下アルファチャンネルのグラデーション*/
        time_S += Time.deltaTime;
            if (render.material.color.a >= 0)
        {
            render.material.color = new Color(render.material.color.r, render.material.color.g, render.material.color.b, (1-(time_S / liveTime)));
        }
        //Debug.Log(time_S);
    }

    public void DestroyTime()
    {
        
        for (int i = 0; i < VertexCnt; i++)
        {
            Destroy(Vertexes[i]);
        }
        Destroy(gameObject);
    }
}
