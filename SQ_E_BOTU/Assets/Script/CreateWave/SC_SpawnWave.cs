﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_SpawnWave : MonoBehaviour {

    public float time = 1.0f;
    private float time_S  = 0f;
    public float liveTime = 2.0f;
    private GameObject ltempInst;
    public GameObject spown;
    private PlayerControl playerControl;
    [SerializeField] private float walkLiveTime = 2.0f;
    [SerializeField] private float walkTime_S = 1.0f;

    [SerializeField] private float DashLiveTime = 4.0f;
    [SerializeField] private float DashTime_S = 0.5f;
	// Use this for initialization
	void Start () {
        playerControl = GetComponent<PlayerControl>();
    }
	
	// Update is called once per frame
	void Update () {
        if (InputMnager.Instance.DashFlag == false)
        {
            liveTime = walkLiveTime;
            time = walkTime_S;
        }
        else
        {
            liveTime = DashLiveTime;
            time = DashTime_S;
        }

        time_S += Time.deltaTime;
        if (time_S >= time && !playerControl.GoalFlag)
        {
            ltempInst = Instantiate(spown, transform.position,Quaternion.identity);
            //ltempInst.transform.parent = this.gameObject.transform;
            ltempInst.GetComponent<SC_CreateWave>().liveTime = liveTime;//ここで波紋の寿命を指定
            time_S = 0.0f;
            SoundManager.Instance.PlaySE(SoundManager.SEenum.LiquidsBubbling,0.2f);
        }
    }

    private void FixedUpdate()
    {

        
    }
}
