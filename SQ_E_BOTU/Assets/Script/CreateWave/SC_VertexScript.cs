﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_VertexScript : MonoBehaviour
{

    [SerializeField]private float speed = 50.0f;
    [SerializeField] private float Dashspeed = 100.0f;
    public bool isEnemyNear = false;
    new Rigidbody2D rigidbody2D;
    private void Start()
    {
        Vector3 direction =transform.up;
        rigidbody2D = this.GetComponent<Rigidbody2D>();
        if (InputMnager.Instance.DashFlag == false)
        {
            rigidbody2D.AddForce(direction * speed);
        }
        else
        {
            rigidbody2D.AddForce(direction * Dashspeed);
        }
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Wall")
        {
            rigidbody2D.isKinematic = true;
            rigidbody2D.velocity = Vector2.zero;

        }
    }
   
}
