﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SC_WingRotate : MonoBehaviour
{


    public float rotate = 36.0f;
    private bool isReturn = false;
    float speed = 60.0f;

    private SC_AIToMoveClick SC_AIToMoveClick;
    public float leapAngle;
    // Use this for initialization
    void Start()
    {
        SC_AIToMoveClick = transform.GetComponentInParent<SC_AIToMoveClick>();

    }

    // Update is called once per frame
    void Update()
    {

        if (SC_AIToMoveClick.playerAimFlag)
        {
            if (!(isReturn))
            {
                isReturn = true;
            }
            transform.Rotate(0.0f, 0.0f, rotate * Time.deltaTime);

        }
        else
        {
            if (isReturn)
            {

                float step = speed * Time.deltaTime;

                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 0), step);


                if (this.transform.rotation.z == 0.0f)
                {
                    isReturn = false;
                    Debug.Log(this.transform.rotation.z);

                }
            }
        }
    }
}
