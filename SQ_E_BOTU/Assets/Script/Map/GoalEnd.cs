﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GoalEnd : MonoBehaviour {

    [SerializeField] private Camera mainCam;

    private BoxCollider2D boxCollider2D;
    public string scename="";

    public GameObject plane;

    public GameObject cameraMoveTo;
    public float cameraSize = 1.0f;
    public float cameraMoveTime = 3.0f;


    [SerializeField] private int sceneFlag = 0;
    private SpriteRenderer spriteRenderer;
    private SpriteRenderer spriteRenderer2;

    private Color color;

    private Vector3 tempVector;
    private float temp;

    // Use this for initialization
    void Start () {
        VideoManager.Instance.SetMainCamToVideo();
        VideoManager.Instance.SetVideoClip("clear");
        VideoManager.Instance.PauseVideo();

        boxCollider2D = gameObject.GetComponent<BoxCollider2D>();
        mainCam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        spriteRenderer = cameraMoveTo.GetComponent<SpriteRenderer>();
        spriteRenderer2 = plane.GetComponent<SpriteRenderer>();

        color = spriteRenderer.color;

    }

    // Update is called once per frame
    void Update () {
        switch (sceneFlag)
        {
            case 1: {
                    tempVector= new Vector3( Mathf.Lerp(mainCam.transform.position.x, cameraMoveTo.transform.position.x, Time.deltaTime  ),
                        Mathf.Lerp(mainCam.transform.position.y, cameraMoveTo.transform.position.y, Time.deltaTime),-10.0f
                        );
                    mainCam.transform.position = tempVector;

                    temp = Mathf.Lerp(mainCam.orthographicSize, 5.3f * cameraSize, Time.deltaTime );
                    mainCam.orthographicSize = temp;

                    break;
                }
            case 2: {
                    color.a = Mathf.Lerp(color.a, 1, Time.deltaTime*2);
                    
                    spriteRenderer.color = color;
                    spriteRenderer2.color = new Vector4(1.0f,1.0f,1.0f, color.a);
                    break;
                }
            case 3:
                {
                    color.a = Mathf.Lerp(color.a, 0, Time.deltaTime * 2);
                    spriteRenderer.color = color;
                    break;
                }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("GOAL");
        SoundManager.Instance.PlaySE(SoundManager.SEenum.GoalEffect,0.1f);
        collision.GetComponent<PlayerControl>().GoalFlag = true;
        collision.GetComponent<CircleCollider2D>().enabled = false;

        mainCam.GetComponent<FollowCamera>().enabled = false;

        Invoke("CameraMove", 0.2f);
        boxCollider2D.enabled = false;

        //SceneChanger.Instance.AddScene("Clear");
        //InputMnager.Instance.OffInputMnager();



    }

    private void CameraMove()
    {
        sceneFlag++;
        Invoke("ActiveBacground", cameraMoveTime);
    }

    private void ActiveBacground()
    {
        sceneFlag++;
        Invoke("DisableBacground", 3.0f);
    }

    private void DisableBacground()
    {
        sceneFlag++;
        Invoke("PlayVideo", 3.0f);
    }
    private void PlayVideo()
    {
        VideoManager.Instance.PlayVideo();
        SoundManager.Instance.PlaySE(SoundManager.SEenum.LiquidsBubbling, 0.5f, 2.0f);
        Invoke("ChangeNextScene", 3f);

    }


    private void ChangeNextScene()
    {
        SceneChanger.Instance.ChangeScene(scename,1.5f);
 
    }

}
