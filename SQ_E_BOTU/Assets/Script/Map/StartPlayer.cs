﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPlayer : MonoBehaviour {

    [SerializeField]
    private GameObject Player;
    private GameObject Camera;
    private void Awake()
    {
       
    }

    // Use this for initialization
    void Start () {
        Player = Resources.Load("Prefab/Player") as GameObject;
        Camera = Resources.Load("Prefab/Camera") as GameObject;
        Instantiate(Player,this.transform.position,Quaternion.identity);
        Instantiate(Camera,new Vector3( this.transform.position.x, this.transform.position.y, -10), Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
