﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {

    [SerializeField] private RectTransform[] imageObject = new RectTransform[4];
    private const float XMax = 2000.0f;
    private const int Count = 40;
    private int imageCount;
    private bool nextFlag;
    // Use this for initialization
    void Start () {
        imageCount = 0;
        nextFlag = true;
    }
	
	// Update is called once per frame
	void Update () {
		if(InputMnager.Instance.Phase == InputMnager.UTouchPhase.Began && nextFlag==true)
        {
            if (imageCount >= imageObject.Length-1)
            {
                //Debug.Log("Tutorial終わり");
                SceneChanger.Instance.ChangeScene("Stage1", 1.5f);
            }
            else
            {
                nextFlag = false;
                StartCoroutine(MoveImage(imageCount));
                imageCount++;
            }
        }
	}
    private IEnumerator MoveImage(int index)
    {
        Image image = imageObject[index].GetComponent<Image>();
        Color color;
        for (int i =0;i< Count; ++i)
        {
            imageObject[index].localPosition = new Vector2(Mathf.SmoothStep(0, XMax, i / (float)(Count - 1)), 0.0f);
            color= image.color;
            color.a = Mathf.Lerp(1, 0, i / (float)(Count - 1));
            image.color = color;
            yield return null;
        }
        nextFlag = true;

    }
}
