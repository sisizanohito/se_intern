﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleEnd : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
        VideoManager.Instance.SetMainCamToVideo();
        VideoManager.Instance.SetVideoClip("title");
        VideoManager.Instance.ForwardVideoFlame();
        VideoManager.Instance.PlayVideo();
        Invoke("PauseVideo", 1.0f);
    }

    // Update is called once per frame
    void Update () {
		if(InputMnager.Instance.Phase == InputMnager.UTouchPhase.Began)
        {
            if (!VideoManager.Instance.IsPlay)
            {
                VideoManager.Instance.PlayVideo();
                SoundManager.Instance.PlaySE(SoundManager.SEenum.LiquidsBubbling, 0.5f,1.0f);
                Invoke("Changescene", 3);
            }
        }
	}

    void Changescene()
    {
        SceneChanger.Instance.ChangeScene("Tutorial",1.5f);

    }
    void PauseVideo()
    {
        VideoManager.Instance.PauseVideo();
    }
}
