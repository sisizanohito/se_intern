﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {
    
    private Vector3 VetorPlayer, VectorCamera;
    [SerializeField]
    private GameObject Player;
    private const float Count=1f;
    
    // Use this for initialization
    void Start () {
        Player = GameObject.FindWithTag("Player");
        VetorPlayer= Player.transform.position ;
        VectorCamera = gameObject.transform.position;

    }
	
	// Update is called once per frame
	void LateUpdate () {
        VetorPlayer = Player.transform.position;
        VectorCamera = gameObject.transform.position;
        VetorPlayer.z = VectorCamera.z;
        //Debug.Log(VetorPlayer);
        gameObject.transform.position = Vector3.MoveTowards(VectorCamera, VetorPlayer,Vector3.Distance(VetorPlayer, VectorCamera)*Count); 


    }
}
