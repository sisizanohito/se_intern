﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIcon : MonoBehaviour {

    private PlayerControl playerControl;
    private GameObject goal;
    private float position;
    private float lerp, lerpPosition;
    private SpriteRenderer spriteRenderer;
    private Color color;
    [SerializeField] private GameObject compass,center;
    [SerializeField]private GameObject[] linePosition = new GameObject[4];
    // Use this for initialization
    void Start () {
        goal = GameObject.FindWithTag("Goal");
        spriteRenderer = compass.GetComponent<SpriteRenderer>();
        playerControl = transform.GetComponentInParent<PlayerControl>();
        position = transform.localPosition.y;
        lerpPosition = 0.0f;
        color = spriteRenderer.color;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        lerp = Mathf.InverseLerp(0, playerControl.GetDashSpeed,playerControl.GetfSpeed)/2;
        lerpPosition=Mathf.Lerp(lerpPosition, lerp, Time.deltaTime*5);
        transform.localPosition = new Vector2(0.0f, position+ lerpPosition);
        if (InputMnager.Instance.DashFlag)
        {
            color.a = Mathf.Lerp(color.a, 0, Time.deltaTime * 5);
        }
        else
        {
            color.a = Mathf.Lerp(color.a, 1, Time.deltaTime * 5);
        }
       
        spriteRenderer.color = color;
        Vector2 crossPoint = new Vector2(0.0f, 0.0f);
        for (int i = 0; i < 4; ++i)
        {
            if (LineSegmentsIntersection(goal.transform.position, center.transform.position
                                               , linePosition[i % 4].transform.position, linePosition[(i + 1) % 4].transform.position,out crossPoint))
            {
                compass.transform.position = crossPoint;
            }
        }
        


    }

    public static bool LineSegmentsIntersection(
    Vector2 p1,
    Vector2 p2,
    Vector2 p3,
    Vector2 p4,
    out Vector2 intersection)
    {
        intersection = Vector2.zero;

        var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);

        if (d == 0.0f)
        {
            return false;
        }

        var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
        var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;

        if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
        {
            return false;
        }

        intersection.x = p1.x + u * (p2.x - p1.x);
        intersection.y = p1.y + u * (p2.y - p1.y);

        return true;
    }
}
